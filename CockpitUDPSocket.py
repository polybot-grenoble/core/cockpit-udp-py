from threading import Thread, Semaphore
import socket
import json
from typing import List, Dict, Callable, Tuple
from uuid import uuid4

class CockpitPacket:
    uid: str = ""
    client: str = ""
    type: str = ""
    payload: str = ""

    def raw (self):
        return json.dumps({
            "uid": self.uid,
            "client": self.client,
            "type": self.type,
            "payload": self.payload
        })
    
    def load (self, raw: str):
        obj = None
        
        try:
            obj = json.loads(raw)
        except json.JSONDecodeError as err:
            print("Failed to parse packet", err.msg)
            return
        
        if "uid" in obj:
            self.uid = obj["uid"]

        if "client" in obj:
            self.client = obj["client"]

        if "type" in obj:
            self.type = obj["type"]

        if "payload" in obj:
            self.payload = obj["payload"]


class CockpitRequest:
    """Blocking request to ease code writing. 
    Copied from Core's `PeripheralRequest`
    
    Returns:
        CockpitRequest: A request
    """

    data: Semaphore     # Semaphore to block until the response is received

    req: CockpitPacket                  # The packet sent as a request
    res: CockpitPacket | None = None    # The packet received as a response

    sent: bool = False          # Was the request sent ?

    def __init__(self, req: CockpitPacket) -> None:
        """Creates a request

        Args:
            req (CockpitPacket): The packet sent as a request
        """
        self.data = Semaphore(value=0)
        self.req = req

    def resolve (self, packet: CockpitPacket) -> None:
        """Resolves this request

        Args:
            packet (CockpitPacket): The response from Core
        """

        self.res = packet
        self.data.release()

    def exec (self, send: Callable[[CockpitPacket], None]) -> CockpitPacket | None:
        """Executes the request

        Args:
            send (Callable[[CockpitPacket], None]): Method used to send the packet

        Returns:
            CockpitPacket | None: The packet, None if timed out
        """

        send(self.req)
        self.data.acquire(timeout=2.0) # float: 1s
       
        return self.res


class CockpitUDPSocket: 
    """Cockpit UDP client

    Returns:
        CockpitUDPSocket: The client
    """

    # This client's ID and port
    id: str                     # This client ID
    port: int                   # This client UDP port
    ipv4: str = "127.0.0.1"     # This client IPv4

    # Flag to control threads
    running: bool = False

    # UDP reader thread
    readingThread: Thread

    # Client's UDP address
    client_ipv4: str
    client_port: int

    # UDP socket
    sock: socket

    # Received packet queue
    input: List[CockpitPacket] = []

    # Requests
    requests: Dict[str, CockpitRequest] = {}

    # Semaphore used when trying to find cockpit
    exec_lock: Semaphore = Semaphore(0)
    cockpit_search_addr: Tuple[str, int] | None = None

    def __init__(self, id: str = "hmi", port: int = 42012) -> None:
        """Creates a new Cockpit UDP client

        Args:
            id (str, optional): Id of this client. Defaults to "hmi". 
            port (int, optional): Port opened by this client. Defaults to 42012. 
        """
        self.id = id
        self.port = port

    def open (self, ipv4: str = "127.0.0.1", port: int = 42042):
        """Opens the socket and defines Core's address and port

        Args:
            ipv4 (str, optional): Core's IPv4. Defaults to "127.0.0.1".
            port (int, optional): Core's UDP Port. Defaults to 42042.
        """

        if self.running:
            return
        self.running = True

        print("Opening socket")

        self.client_ipv4 = ipv4
        self.client_port = port

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind((self.ipv4, self.port))

        self.readingThread = Thread(target=self.reader, args=[])
        self.readingThread.start()

    def reader (self):
        """Reader Thread code
        """

        print("Started reader thread")

        while self.running:
            data, addr = self.sock.recvfrom(4096)
            
            if len(data) < 5:
                print("Invalid frame size (size < header)")
                print(data, addr)
                continue
            
            op_code = int(data[0])
            size = int.from_bytes(data[1:4], "little")
            payload = data[5:-1].decode("utf-8")

            self.process_frame(op_code, payload, addr)

        print("Stopping reader thread")

    def process_frame (self, op_code: int, payload: str, client):
        """Processes the received frame

        Args:
            op_code (int): The type of frame
            payload (str): The payload
        """
        p = CockpitPacket()

        if op_code == 0:
            # An announce packet, ignoring
            print("Recieved announce from", payload, "@", client)
            self.cockpit_search_addr = client
            return
        
        if op_code == 1:
            # A JSON string representing a packet
            p.load(payload)
        
        elif op_code == 2:
            # A raw string as a response
            p.client = "core"
            p.type = "ok"
            p.payload = payload

        # Check if this is a response to a request
        if p.uid in self.requests:
            self.requests[p.uid].resolve(p)

        # We put the packet in the input queue
        else:
            self.input.append(p)


    def stop (self):
        """Stops this client
        """

        print("Stopping Cockpit client")
        self.running = False

        self.sock.sendto(b'\0', (self.ipv4, self.port))

        self.readingThread.join()

    def make_frame (self, op_code: int, payload: str) -> bytes:
        """Creates a Cockpit UDP frame to send to Core

        Args:
            op_code (int): Type of frame
            payload (str): Payload to send

        Returns:
            bytes: The binary data to send to Core
        """
        size = len(payload) + 1
        
        frame = bytearray()
        frame.extend(int.to_bytes(op_code, 1, "little", signed=False))
        frame.extend(int.to_bytes(size, 4, "little", signed=False))
        frame.extend(payload.encode("utf-8"))
        frame.extend(b'\0')

        return frame
    
    def announce (self):
        """Sends an announce frame to Core
        """
        frame = self.make_frame(0, self.id)
        self.sock.sendto(frame, (self.client_ipv4, self.client_port))

    def sendPacket (self, packet: CockpitPacket):
        """Sends a Packet to Core

        Args:
            packet (CockpitPacket): The packet to send
        """
        frame = self.make_frame(1, packet.raw())
        self.sock.sendto(frame, (self.client_ipv4, self.client_port))

    def sendString (self, payload: str):
        """Sends a raw string to Core

        Args:
            payload (str): The raw string
        """
        frame = self.make_frame(2, payload)
        self.sock.sendto(frame, (self.client_ipv4, self.client_port))

    def send (self, type: str, payload: str, uid: str = ""):
        """Creates and sends a packet to core

        Args:
            type (str): Type of packet
            payload (str): Payload of the packet
            uid (str, optional): Unique identifier for the packet. Defaults to "".
        """
        packet = CockpitPacket()
        packet.client = self.id
        packet.payload = payload
        packet.type = type
        packet.uid = uid
        
        self.sendPacket(packet)

    def request (self, payload: str) -> CockpitPacket | None:
        """Sends a request to Core and blocks the current thread until the response is received.

        Args:
            payload (str): Command

        Returns:
            CockpitPacket | None: The received response, None if timed out
        """

        packet = CockpitPacket()
        packet.client = self.id
        packet.uid = str(uuid4())
        packet.type = "request"
        packet.payload = payload

        request = CockpitRequest(packet)
        self.requests[packet.uid] = request

        response = request.exec(lambda packet: self.sendPacket(packet))
        del self.requests[packet.uid]

        if response is None:
            # Maybe Core restarted ?
            self.announce()

        return response
    
    def find_core (self):

        i = 0
        while i < 10:   # We have 10 attempts
            self.announce()
            self.exec_lock.acquire(timeout=0.1)
            if self.cockpit_search_addr is not None:
                break
            i += 1

        return self.cockpit_search_addr
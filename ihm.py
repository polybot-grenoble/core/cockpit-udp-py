from threading import Thread
from time import sleep
from pretty import PrettyPrint
import serial 

from CockpitUDPSocket import *

# session = PromptSession()
run = True
pretty = PrettyPrint()
arduino = serial.Serial(port='/dev/ttyUSB0', baudrate=115200, timeout=0.1) #verifie le timeout speed

def read ():
    try:
        t = session.prompt("> ")
    except EOFError:
        t = "exit"
    except KeyboardInterrupt:
        t = "exit"
    return t

def printer_thread (cockpit: CockpitUDPSocket):
    
    while run:
        to_print = len(cockpit.input)
        if to_print == 0:
            sleep(0.01)

        while to_print > 0:
            packet = cockpit.input.pop(0)
            print(pretty.print(packet))
            to_print -= 1

# -------- ajout de methode --------------#
def send(donnees_RASP):
    tab_RASP = donnees_RASP.split(";")
    cmd = tab_RASP[0]
    val = tab_RASP[1]
    # print(cmd)
    # print(val)
    arduino.writelines(cmd + ";" + val)

def send_V2(donnees_RASP_V2: CockpitPacket):
    if(donnees_RASP_V2.type == "talos_state"):
        arduino.writelines('t' + ";" + donnees_RASP_V2.payload)
    elif(donnees_RASP_V2.type == "periph_list"):
        arduino.writelines('l' + ";" + donnees_RASP_V2.payload)
#----------------------------------------#

if __name__ == "__main__":
    # Getting basic infos
    port = 42012 #int(session.prompt("CLI Port : ", default="42000"))
    name = "hmi" # session.prompt("CLI Name : ", default="cli")

    # Validating port
    port = port if (port > 5000 and port < 65535) else 42000

    # Opening the UDP socket
    cockpit = CockpitUDPSocket(name, port)
    cockpit.open("0.0.0.0")

    # Getting Core's address
    addr = cockpit.find_core()
    
    if addr is None:
        print("Unable to find Core, quitting ...")
        cockpit.stop()
        exit(1)
    else:
        print("Found Core at", addr)

    cockpit.client_ipv4 = addr[0]
    cockpit.client_port = addr[1]

    # Starting printer thread
    printer = Thread(target=printer_thread, args=[cockpit], daemon=False)
    printer.start()

    # -------- partie modifier ---------------#
    donnees_RASP = ""
    data = arduino.readline() 
    # Getting user input
    while(arduino.readable and arduino.writable and data != "exit"): 
        try:
            data = arduino.readline().decode("ascii")
        except:
            continue
        response = cockpit.request(data)
        if response is None:
            print("Error: Request timed out")
        else:
            if len(data) > 2:
                print(data)
            send_V2(response)

        if(donnees_RASP != ""):
            send(donnees_RASP)
        donnees_RASP = ""
    #-----------------------------------------#

    # Ending program
    cockpit.stop()
    
    run = False
    printer.join()

from typing import List
from functools import reduce

# Dark magic that I used to write in Typescript around August 2023.
# I don't remember how it works, and I don't want to retro-engineer it.

def format_section (section: List[List[str]]):

    col_number = len(section[0])

    def _transform_section (line: List[str], col_width, i):
        line[i] = line[i].ljust(col_width)
        return line

    for i in range(col_number):

        col_width = reduce((lambda p, s: max(p, len(s[i]))), section, 0)
        section = list(map((lambda l: _transform_section(l, col_width, i)), section))

    return list(map((lambda ln: " ".join(ln)), section))

def niceBox (rawTitle: str, rawSections: List[List[List[str]]], style: str = ""):

    title = f" {rawTitle} "
    min_w = len(title) + 4

    sections = list(map(format_section, rawSections))
    min_w = reduce((lambda w, s: max(w, len(s[0]))), sections, min_w)

    # Using temp vars due to python's stupidity
    _temp = ("\u2500" * (min_w + 2))
    sep_line = f"\n{style}\u251c{_temp}\u2524\x1b[0m\n"

    def map_line (ln: str):
        return f"{style}\u2502\x1b[0m {ln.ljust(min_w)} {style}\u2502\x1b[0m"

    def map_section (section: List[str]):
        return f"\n".join(list(map(map_line, section)))

    body = sep_line.join(list(map(map_section, sections)))

    half_title_width = (min_w - len(title)) // 2
    remainder = (min_w - len(title)) % 2
    
    # Using temp vars due to python's stupidity
    _temp_a = "\u2500" * (half_title_width + remainder)
    _temp_b = "\u2500" * half_title_width
    top_line = f"\n{style}\u256d{_temp_a}\u2574{title}\u2576{_temp_b}\u256e\x1b[0m\n"

    # Using temp vars due to python's stupidity
    _temp = "\u2500" * (min_w + 2)
    bottom_line = f"\n{style}\u2570{_temp}\u256f\x1b[0m\n"

    return top_line + body + bottom_line

from prompt_toolkit import PromptSession, ANSI, shortcuts
from prompt_toolkit import print_formatted_text as print
from threading import Thread
from time import sleep
from pretty import PrettyPrint

from CockpitUDPSocket import *

session = PromptSession()
run = True
pretty = PrettyPrint()

def read ():
    try:
        t = session.prompt("> ")
    except EOFError:
        t = "exit"
    except KeyboardInterrupt:
        t = "exit"
    return t

def printer_thread (cockpit: CockpitUDPSocket):
    
    while run:
        to_print = len(cockpit.input)
        if to_print == 0:
            sleep(0.01)

        while to_print > 0:
            packet = cockpit.input.pop(0)
            print(ANSI(pretty.print(packet)))
            to_print -= 1


if __name__ == "__main__":
    # Getting basic infos
    port = int(session.prompt("CLI Port : ", default="42000"))
    name = session.prompt("CLI Name : ", default="cli")

    # Validating port
    port = port if (port > 5000 and port < 65535) else 42000

    # Opening the UDP socket
    cockpit = CockpitUDPSocket(name, port)
    cockpit.open("0.0.0.0")

    # Getting Core's address
    addr = cockpit.find_core()
    
    if addr is None:
        print("Unable to find Core, quitting ...")
        cockpit.stop()
        exit(1)
    else:
        print("Found Core at", addr)

    cockpit.client_ipv4 = addr[0]
    cockpit.client_port = addr[1]

    # Starting printer thread
    printer = Thread(target=printer_thread, args=[cockpit], daemon=False)
    printer.start()

    def opcode (txt: str):
        oc = txt[1:]
        match (oc):
            case "ping":
                cockpit.request("version")
            case "clear":
                shortcuts.clear()
            case "cls":
                shortcuts.clear()
            case "manette":
                cockpit.request("peripheral reload")
                sleep(2)
                cockpit.request("strat set_team 1")
                sleep(1)
                cockpit.request("strat select manual_udp")
                sleep(1)
                cockpit.request("talos start")
                sleep(1)
                cockpit.request("talos tirette_on")
                sleep(1)
                cockpit.request("talos tirette_off")
            case _:
                print("No opcode " + oc)

    # Getting user input
    txt = read()
    while txt != "exit":

        if len(txt) > 0 and txt[0] == "!":
            opcode(txt)
            txt = read()
            continue

        response = cockpit.request(txt)
        if response is None:
            print("Error: Request timed out")
        else:
            print(ANSI(pretty.print(response)))

        txt = read()

    # Ending program
    cockpit.stop()
    
    run = False
    printer.join()
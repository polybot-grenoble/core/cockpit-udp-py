from typing import Callable
from CockpitUDPSocket import CockpitPacket, Dict

import pretty_functions

PrintHook = Callable[[CockpitPacket], str]

class PrettyPrint:
    """A class used to render packets based on their type
    """

    # Functions used to render a specific type of packet
    _hooks: Dict[str, PrintHook] = {}

    def __init__(self) -> None:
        """Initializes PrettyPrint
        """

        self.hook("error", pretty_functions.print_error)
        self.hook("help", pretty_functions.print_help)
        self.hook("version", pretty_functions.print_version)
        self.hook("talos_state", pretty_functions.print_talos_state)
        self.hook("log", pretty_functions.print_log)
        self.hook("log_error", pretty_functions.print_log_error)
        self.hook("log_debug", pretty_functions.print_log_debug)
        self.hook("periph_list", pretty_functions.print_periph_list)
        self.hook("tps", pretty_functions.print_tps)
        self.hook("strat_list", pretty_functions.print_strat_list)

        self.hook("*", pretty_functions.print_packet)

        pass

    def hook (self, type: str, hook: PrintHook) -> None:
        """Adds a print function for a packet type

        Args:
            type (str): Packet type to print
            hook (PrintHook): Function to print the packet type
        """
        self._hooks[type] = hook

    def print (self, packet: CockpitPacket) -> str:
        """Formats a packet to be printed

        Args:
            packet (CockpitPacket): The packet

        Returns:
            str: It's nice representation
        """

        if packet.type in self._hooks:
            return self._hooks[packet.type](packet)
        
        if "*" in self._hooks:
            return self._hooks["*"](packet)
        
        return self.basic_print(packet)
    
    def basic_print (self, packet: CockpitPacket):
        return f"({packet.type}) {packet.payload}"
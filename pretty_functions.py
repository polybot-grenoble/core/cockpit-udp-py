from CockpitUDPSocket import CockpitPacket
import json
from box import niceBox
from datetime import datetime
from dateutil import tz

TZ = tz.gettz("Europe/Paris")

def rgb (r, g, b):
    return f"\x1b[38;2;{r};{g};{b}m"

def print_error (packet: CockpitPacket):

    msg = json.loads(packet.payload)
    code = msg["code"]

    return niceBox(
        f"Error code {code}",
        [[msg["message"].replace("\r", "").split("\n")]],
        rgb(0xff,0,0)
    )

def print_help (packet: CockpitPacket):

    def format_arg (arg):
        name = arg["name"]
        return f"<{name}>" if arg["required"] else f"[{name}]"

    def format_line (ln):
        args = ln["args"]
        arg_str = ""
        if len(args) > 0:
            arg_str = " " + " ".join(list(map(format_arg, args)))
        id = ln["id"]

        return [f"{id}{arg_str}", ln["description"]]
    
    help = json.loads(packet.payload)
    
    title = help["title"] + " - " + help["description"]

    routes = list(map(format_line, list(filter(lambda ln: ln["type"] == "route", help["lines"]))))
    cmds = list(map(format_line, list(filter(lambda ln: ln["type"] == "cmd", help["lines"]))))
    
    msg = []
    if len(routes):
        msg.append(routes)
    if len(cmds):
        msg.append(cmds)

    return niceBox(title, msg, rgb(4,147,255))

def print_version (packet: CockpitPacket):

    return niceBox(
        "Version",
        [[[packet.payload]]],
        rgb(0xcc, 0xcc, 0xcc)
    )

def print_talos_state (packet: CockpitPacket):

    return niceBox(
        "Etat Talos",
        [[[packet.payload]]],
        rgb(0xcc, 0x00, 0x77)
    )

def print_packet (packet: CockpitPacket):
    return niceBox(
        packet.type,
        [[[packet.payload]]],
        rgb(0xee,0xee,0xff)
    )

def print_tps (packet: CockpitPacket):
    tps = float(packet.payload)
    L = 10
    pct = round(min(1, tps / 50) * L)
    return "\n[" + "=" * pct + " " * (L - pct) + "] " + str(round(tps)) + " ticks/s\n"

def print_log (packet: CockpitPacket):
    date = datetime.now(TZ).strftime("[%H:%M:%S] ")
    return date + "\x1b[32m[INFO ]\x1b[0m " + packet.payload

def print_log_error (packet: CockpitPacket):
    date = datetime.now(TZ).strftime("[%H:%M:%S] ")
    return date + "\x1b[31m[ERROR]\x1b[0m " + packet.payload

def print_log_debug (packet: CockpitPacket):
    date = datetime.now(TZ).strftime("[%H:%M:%S] ")
    return date + "\x1b[37m[DEBUG]\x1b[0m " + packet.payload

def print_periph_list (packet: CockpitPacket):
    peripherals = packet.payload.split(";")[:-1]
    if (len(peripherals) == 0):
        return "\nNo peripherals\n"
    peripherals = peripherals

    def explode (line: str):
        [a, status] = line.split("@")
        [family, hermesID] = a.split(":")

        return [hermesID, family, status]
    
    liste = [explode(p) for p in peripherals]
    
    return niceBox(
        "Peripherals",
        [[
            [ "ID", "Family", "Status" ],
            [ "--", "------", "------" ],
            *liste
        ]],
        rgb(0xff,0xff,0x00)
    )

def print_strat_list (packet: CockpitPacket):
    strats = packet.payload.split(";")[:-1]
    if (len(strats) == 0):
        return "\nNo strategie available\n"
    strats = [[s] for s in strats]

    return niceBox(
        "Strategies",
        [strats],
        rgb(0x55, 0xff, 0xcc)
    )

if __name__ == "__main__":
    # Test - Error packet
    p = CockpitPacket()
    p.payload = '{"code": 12, "message": "IESE !!!"}'
    print(print_error(p))

    # Test - Help message
    p.payload = """{"description":"The robot's brain","lines":[{"args":[],"description":"Shows the help message","id":"help","type":"cmd"},{"args":[],"description":"Stops core","id":"stop","type":"cmd"},{"args":[],"description":"Shows Core's version","id":"version","type":"cmd"},{"args":[],"description":"A test sub-route","id":"sub","type":"route"}],"title":"Core"}"""
    print(print_help(p))

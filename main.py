from CockpitUDPSocket import *

def read ():
    try:
        t = input("> ")
    except EOFError:
        t = "exit"
    except KeyboardInterrupt:
        t = "exit"
    return t

cockpit = CockpitUDPSocket()

cockpit.open("0.0.0.0")

cockpit.announce()

t = read()
while t != "exit":
    res = cockpit.request(t)
    if res is None:
        print("Request timed out")
    else:
        print(f"[{res.type}] {res.payload}")

    t = read()

cockpit.stop()
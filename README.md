# Cockpit UDP Python

Ceci est l'implémentation python de Cockpit UDP. 

## `CockpitUDPSocket.py`

Comment utiliser Cockpit ?
Rien de plus simple !

Il vous faudra instancier la classe `CockpitUDPClient`.

```py
cockpit = CockpitUDPSocket("hmi", 42012)
```

Ensuite, ouvrez Cockpit.
Cela crée un Thread qui va lire le socket UDP, et va stocker les paquets (JSON)
reçus dans sa variable `input`.

```py
cockpit.open()
```

Si vous le souhaitez, vous pouvez rechercher Core sur le réseau local dans 
le cas où vous ne connaissez pas son addresse IPv4.

```py
cockpit.open("0.0.0.0")

# Getting Core's address
addr = cockpit.find_core()

if addr is None:
    print("Unable to find Core, quitting ...")
    cockpit.stop()
    exit(1)
else:
    print("Found Core at", addr)
```

Pour lire les paquets entrants que vous n'avez pas demandé (oui il y en a 
beaucoup), il est conseillé de créer un Thread qui s'occupe de ceux-ci.

```py
run = True

...

def printer_thread (cockpit: CockpitUDPSocket):
    
    while run:
        to_print = len(cockpit.input)
        if to_print == 0:
            sleep(0.01)

        while to_print > 0:
            packet = cockpit.input.pop(0)
            # Remplacez print(...) par ce que vous voulez pour traiter le paquet
            print(packet.type, packet.payload)
            to_print -= 1

...

# Création du Thread
printer = Thread(target=printer_thread, args=[cockpit], daemon=False)
printer.start()

...

# En fin de programme, on termine l'execution du thread
run = False
printer.join()
```

Pour effectuer une demande auprès de Cockpit, il suffit d'envoyer une requête 
à l'aide de `cockpit.request(...)` :

```py
response = cockpit.request("help")
if response is None:
    print("Error: Request timed out")
else:
    print(response.type, response.payload)
```

Cette fonction est bloquante mais possède un timeout d'une seconde.

Enfin à la fin du programme il faut stopper le client Cockpit :

```py
cockpit.stop()
```

# `cli.py` - L'interface en lignes de commande

**Ce script requiert la librairie `prompt_toolkit`.**

Afin de permettre un contrôle plus avancé de Core, cet utilitaire permet
d'envoyer des requêtes à Core manuellement et de voir le résultat s'afficher 
à la reception de la réponse.

Au démarrage, le nom de l'interface ainsi que le port UDP à utiliser seront 
demandés. Cela permet d'ouvirir plusieurs interfaces en même temps. 

Si vous redémarrez Core, il vous faudra redémarrer toutes les interfaces 
encore ouvertes afin d'assurer leur bon fonctionnement.
